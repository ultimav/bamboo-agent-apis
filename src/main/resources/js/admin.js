

(function ($) { // this closure helps us keep our variables to ourselves.
// This pattern is known as an "iife" - immediately invoked function expression


    // form the URL
    var url = AJS.contextPath() + "/rest/agent-config/1.0/admin-config";

    // wait for the DOM (i.e., document "skeleton") to load. This likely isn't necessary for the current case,
    // but may be helpful for AJAX that provides secondary content.
    $(document).ready(function() {
        // request the config information from the server
        $.ajax({
            url: url,
            dataType: "json"
        }).done(function(config) { // when the configuration is returned...
            // ...populate the form.
        	console.log(config);
            $("#stance").val(config.stance);
            $("#maxactive").val(config.maxactive);
            $("#targetVersion").val(config.targetVersion);
            $("#stance").trigger("change");
        });
        
    	AJS.InlineDialog(AJS.$("#popupLink"), 1,
    	    function(content, trigger, showPopup) {
    	        content.css({"padding":"20px"}).html($('#popupHelpBody').html() );
    	        showPopup();
    	        return false;
    	    }
    	);
    	
    });

})(AJS.$ || jQuery);


function updateConfig() {
	var selectedEnum = AJS.$("#stance").val();
	console.log(selectedEnum);
	var data = '{ "stance": "' + selectedEnum + '", "maxactive": "' +  AJS.$("#maxactive").attr("value") + '", "targetVersion": "' +  AJS.$("#targetVersion").attr("value") + '" }';
	console.log(data);
	  AJS.$.ajax({
	    url: AJS.contextPath() + "/rest/agent-config/1.0/admin-config",
	    type: "PUT",
	    contentType: "application/json",
	    data: data,
	    processData: false,
	    success: function(data) {
	         AJS.messages.success({
	            title: "Saved!",
	            body: "Config updated."
	         }); 
	     }	
	    
	  });
	}
AJS.$(document).ready(function() {
	AJS.$("#agentApiConfigForm").submit(function(e) {
		console.log("SUBMITING!");
	    e.preventDefault();
	    updateConfig();
	});
	AJS.$("#stance").change(function(e) {
		console.log(AJS.$("#stance").val());
	});
});