<html>
  <head>
  <meta name="decorator" content="atl.admin">
  ${webResourceManager.requireResource("com.atlassian.auiplugin:ajs")}
  ${webResourceManager.requireResource("com.atlassian.auiplugin:aui-table-sortable")}
  ${webResourceManager.requireResource("com.edwardawebb.bamboo-agent-apis:agent-apis-token-resources")}
    <title>${i18n.getText("agent-apis.admin.tokens.title")}</title>
  </head>
  <body>
  [@ui.header pageKey="agent-apis.token.admin.heading" /]
	<button class="aui-button aui-button-subtle" href="#" id="popupLink">
	    <span class="aui-icon aui-icon-small aui-iconfont-help">help</span> What is this page?
	    <div id="popupHelpBody" style="display:none;">${i18n.getText("agent-apis.admin.tokens.page.info.popup")}</div>
	</button>
  
    <div id="aui-message-bar"></div>
    
    
    <!-- list existing -->
    <div class="tokenlist">
    	<table class="aui aui-table-sortable" width="90%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>UUID</th>
                    <th>Can Read</th>
                    <th>Can Change</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id="tokenRows">
              
            </tbody>
        </table>
    </div>
    
    <!-- create new -->
    <form id="agentApiTokens" class="aui">
    
       <fieldset>
	      [@ww.textfield id="newTokenName"  descriptionKey="agent-apis.admin.tokens.name.description" labelKey="agent-apis.admin.tokens.name" cssClass="aui" required='true' name="name"/]
	      [@ww.checkbox id="newTokenCanRead" descriptionKey="agent-apis.admin.tokens.read.description" labelKey="agent-apis.admin.tokens.read" cssClass="aui" required='true' name="read"/]
	      [@ww.checkbox  id="newTokenCanChange" descriptionKey="agent-apis.admin.tokens.change.description" labelKey="agent-apis.admin.tokens.change" cssClass="aui" required='true' name="change"/]
	   </fieldset>
      
      <div>
        <input type="submit" value="Create" class="aui-button aui-button-primary">
      </div>
    </form>
    
    
    
  </body>
</html>