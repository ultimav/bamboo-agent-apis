/**
 * 
 */
package com.edwardawebb.bamboo.agentapis.ao;

import java.util.UUID;

import net.java.ao.ActiveObjectsException;
import net.java.ao.DBParam;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;
import com.edwardawebb.bamboo.agentapis.services.audit.AgentApiAuditService;

/**
 * @author Eddie Webb
 * 
 */
public class DefaultAccessTokenService implements AccessTokenService {

    private final ActiveObjects ao;
    private AgentApiAuditService auditService;

    public DefaultAccessTokenService(ActiveObjects activeObjects, AgentApiAuditService agentApiAuditService) {
        this.ao = activeObjects;
        this.auditService = agentApiAuditService;
    }

    @Override
    public AccessToken[] findAll() {
        return ao.find(AccessToken.class);
    }
    
    @Override
    public AccessToken findTokenByUuid(UUID uuid) {
        String uuidValue = uuid.toString();
        AccessToken[] tokens = ao.find(AccessToken.class, Query.select().where("UUID = ?",uuidValue));
        if (null != tokens && tokens.length > 1) {
            throw new ActiveObjectsException("Query returned not exactly 1 result for a unique field. (Count: " + tokens.length +")");
        }

        if (tokens.length == 1) {
            return tokens[0];
        } else {
            return null;
        }
    }
    
    
    @Override
    public AccessToken createNewToken(TokenResource resource) {
        AccessToken token = createNewToken(resource.getName(), resource.isAllowedRead(), resource.isAllowedChange());
        auditService.createTokenEvent(token);
        return token;
    }

    @Override
    public AccessToken createNewToken(final String name, final boolean canRead, final boolean canChange) {
        return ao.executeInTransaction(new TransactionCallback<AccessToken>()
                {
                    @Override
                    public AccessToken doInTransaction()
                    {
                        UUID uuid = UUID.randomUUID();
                        
                        AccessToken token = findTokenByUuid(uuid);
                        if (null != token) {
                            throw new ActiveObjectsException("Entity with UUID: " + uuid
                                    + " already exists. This is a severe error as UUIDs should be unique, please contact support");
                        }            
                        token = ao.create(AccessToken.class, 
                                new DBParam("UUID", uuid.toString()),
                                new DBParam("READ", canRead), 
                                new DBParam("CHANGE", canChange), 
                                new DBParam("NAME", name));
                        return token;
                    }
                });                     
    }

    


    
    
    
    @Override
    public AccessToken updateToken(final int id,final UUID uuid,final String name,final boolean canRead,final boolean canChange) {
        return ao.executeInTransaction(new TransactionCallback<AccessToken>()
                {
                    @Override
                    public AccessToken doInTransaction()
                    {
                        AccessToken token = ao.get(AccessToken.class, id);

                        if (null == token) {
                            throw new ActiveObjectsException("Expected token with ID: " + id + " could not be found.");
                        }
                
                        token.setUuid(uuid.toString());
                        token.setAllowedToRead(canRead);
                        token.setAllowedToChange(canChange);
                        token.setName(name);
                        token.save();
                
                        return token;
                    }
               });
    }

    @Override
    public AccessToken updateToken(final TokenResource resource) {
        
        return ao.executeInTransaction(new TransactionCallback<AccessToken>()
                {
                    @Override
                    public AccessToken doInTransaction()
                    {
                        AccessToken token = findTokenByUuid(UUID.fromString(resource.getUuid()));
                        auditService.updateTokenEvent(token,resource);
                        token.setAllowedToChange(resource.isAllowedChange());
                        token.setAllowedToRead(resource.isAllowedRead());
                        token.setName(resource.getName());
                        token.save();
                        return token;
                    }
                });
    }

    

    
    @Override
    public void purgeToken(final UUID uuid) {
        ao.executeInTransaction(new TransactionCallback<Object>()
                {
                    @Override
                    public AccessToken doInTransaction()
                    {
                        AccessToken token = findTokenByUuid(uuid);
                        auditService.deleteTokenEvent(token);
                        ao.delete(token);
                        return null;
                    }                    
                });
    }
 

}
