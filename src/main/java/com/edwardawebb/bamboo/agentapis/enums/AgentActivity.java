package com.edwardawebb.bamboo.agentapis.enums;

public enum AgentActivity {
    
    UPGRADE("An agent requested to be take offline while it updates local binaries and configuration");
    
   
    
    
    
    
    private String description;
    
    
    AgentActivity(String description){
        this.description = description;
    }

}
