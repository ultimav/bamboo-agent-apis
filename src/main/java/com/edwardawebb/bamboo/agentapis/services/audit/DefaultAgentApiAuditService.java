package com.edwardawebb.bamboo.agentapis.services.audit;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.bamboo.persister.AuditLogService;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.ao.model.AgentInProgress;
import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;

public class DefaultAgentApiAuditService implements AgentApiAuditService {
    private AuditLogService auditLogService;
    private UserManager userManager;

    private static final String FIELD_PREFIX = "Agent.Api.Token.";

    private static final String FIELD_PREFIX_ACTIVITY = "Agent.Api.Activity.";

    public DefaultAgentApiAuditService(AuditLogService auditLogService, UserManager userManager) {
        this.auditLogService = auditLogService;
        this.userManager = userManager;
    }

    @Override
    public void createTokenEvent(AccessToken token) {
        auditLogService.log(FIELD_PREFIX + token.getUuid() + "\n[CREATED]", "",token.getUuid(),null);
        
    }

    @Override
    public void deleteTokenEvent(AccessToken token) {        
        auditLogService.log(FIELD_PREFIX + token.getUuid() + "\n[DELETED]", token.getUuid(),"",null);
    }

    @Override
    public void updateTokenEvent(AccessToken oldToken, TokenResource newValues) {
        if( ! oldToken.getName().equals(newValues.getUuid())){
            auditLogService.log(FIELD_PREFIX + oldToken.getUuid() + "\n[Name]", oldToken.getName(),newValues.getName(),null);
        } 
        if( ! oldToken.isAllowedToRead() == newValues.isAllowedRead()){
            auditLogService.log(FIELD_PREFIX + oldToken.getUuid() + "\n[Read Access]", ""+oldToken.isAllowedToRead(),""+newValues.isAllowedRead(),null);
        } 
        if( ! oldToken.isAllowedToChange() == newValues.isAllowedChange()){
            auditLogService.log(FIELD_PREFIX + oldToken.getUuid() + "\n[Change Access]", ""+oldToken.isAllowedToChange(),""+newValues.isAllowedChange(),null);
        } 
    }
    
    
    private String buildDiffMessage(AccessToken oldToken, TokenResource newValues) {
        StringBuilder sb = new StringBuilder();
        if( ! oldToken.getName().equals(newValues.getUuid())){
            sb.append("Name changed from '").append(oldToken.getName()).append("' to '").append(newValues.getName()).append("\n");
        } 
        if( ! oldToken.isAllowedToRead() == newValues.isAllowedRead()){
            if(oldToken.isAllowedToRead()){
                sb.append("Read access revoked").append("\n");
            }else{
                sb.append("Read access granted").append("\n");
            }
        } 
        if( ! oldToken.isAllowedToChange() == newValues.isAllowedChange()){
            if(oldToken.isAllowedToChange()){
                sb.append("Write access revoked").append("\n");
            }else{
                sb.append("Write access granted").append("\n");
            }
        } 
        return sb.toString();
    }

    private User getUser() {
        String name = userManager.getRemoteUsername();
        if (StringUtils.isEmpty(name)){
            name = "Unkown !!";
        }
        return  new DefaultUser(name);
    }

    @Override
    public void startAcvityEvent(AgentInProgress record) {
        auditLogService.log(FIELD_PREFIX_ACTIVITY + record.getUuid() + "\n[" + record.getActivity() + "]", "",record.getUuid(),null);
        
        
    }

}
