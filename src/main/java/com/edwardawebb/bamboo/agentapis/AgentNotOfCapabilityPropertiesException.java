package com.edwardawebb.bamboo.agentapis;

/**
 * This error should be thrown when an agent {@PipelineDefinition} is not implementing
 * the {@link CapabilityProperties} interface required to enumerate capabilityset
 * @author Eddie Webb
 *
 */
public class AgentNotOfCapabilityPropertiesException extends RuntimeException {

}
